Assessment Loading time.

Summary :- This program is used to measure the loading time(Seconds) of assessments.

Prerequisite before running the program.
1.	User should have machine windows 7 or later.
2.	JRE 1.8 should be installed and configured. If not installed follow the link to install 
https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html
3.	Chrome latest browser.
4.	Internet connection.
5.	If user wants to use different logins for teacher and student in SLZ system. They can modify same in messages.properties file. Strings in the bold can be modified. Make sure that student and teacher is subscribe to PR1ME Mathematics Digital Assessments product. This product contains Performance_Testing_student Assessment
URL=https://qa-slz2.scholasticlearningzone.com/slz-portal/\#/login3/AUSX99Y
TeacherLogin=peterpan12
Password=welcome1
StudentLogin=student
StudentPassword=welcome1


Step to follow to run the file
-	User should download the file Assessment_loading_test.zip.
-	User need to unzip the file Assessment_Pre.zip. Open the folder Assessment_loading_test
-	Inside Assessment_Pre folder. User should see bellow files.
-	User should double click on Assessment_loading_test.jar file. And let the program run.
-	After the completion of the program. User should see a new file(Result.txt) getting created. After clicking on Results.txt. User should see following data.
-	To run the file on Linux machine fun following commands 
chmod +x run.sh
./run.sh
